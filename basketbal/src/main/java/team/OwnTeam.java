package team;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import persoon.*;

@XmlRootElement(name = "thuisploeg")
@XmlAccessorType(XmlAccessType.FIELD)
public class OwnTeam extends Team {

  private String teamCode;

  public OwnTeam() {
    super();
  }

  private String poule;
  private String competitie;
  // @XmlAttribute
  // private int id;
  @XmlAttribute
  private String iss;

  // @XmlElement
  // private String club;
  @XmlElement (name = "teamafkorting")
  private String teamAfkorting;
  @XmlElement(name = "teamnr")
  private int teamNummer;
  @XmlElement
  private String shirt;
  @XmlElement(name = "naam")
  private String teamNaam;
  private Persoon coach;

  public OwnTeam(String teamNaam, String teamCode, String poule, String competitie, Persoon coach) {
    super(teamNaam, teamCode, poule, competitie);
    this.coach = coach;
  }

  /**
   * @return the coach
   */
  public Persoon getCoach() {
    return coach;
  }

  /**
   * @param coach the coach to set
   */
  public void setCoach(Persoon coach) {
    this.coach = coach;
  }

  @Override
  public String toString() {
    return "OwnTeam [teamCode=" + teamCode + ", poule=" + poule + ", competitie=" + competitie + ", id=" + id + ", iss="
	+ iss + ", club=" + club + ", teamAfkorting=" + teamAfkorting + ", teamNummer=" + teamNummer + ", shirt="
	+ shirt + ", teamNaam=" + teamNaam + ", coach=" + coach + "]";
  }

  public String getTeamNaam() {
    return teamNaam;
  }

  public void setTeamNaam(String teamNaam) {
    this.teamNaam = teamNaam;
  }

  public String getTeamCode() {
    return teamCode;
  }

  public void setTeamCode(String teamCode) {
    this.teamCode = teamCode;
  }

  public String getPoule() {
    return poule;
  }

  public void setPoule(String poule) {
    this.poule = poule;
  }

  public String getCompetitie() {
    return competitie;
  }

  public void setCompetitie(String competitie) {
    this.competitie = competitie;
  }

  public String getTeamAfkorting() {
    return teamAfkorting;
  }

  public void setTeamAfkorting(String teamAfkorting) {
    this.teamAfkorting = teamAfkorting;
  }

  public int getTeamNummer() {
    return teamNummer;
  }

  public void setTeamNummer(int teamNummer) {
    this.teamNummer = teamNummer;
  }

  public String getShirt() {
    return shirt;
  }

  public void setShirt(String shirt) {
    this.shirt = shirt;
  }

  public String getIss() {
    return iss;
  }

  public void setIss(String iss) {
    this.iss = iss;
  }
}
