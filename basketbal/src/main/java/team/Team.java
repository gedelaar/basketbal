package team;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public abstract class Team {

  public Team() {
    super();
  }

  protected String teamNaam;
  protected String teamCode;
  protected String poule;
  protected String competitie;
  @XmlAttribute
  protected int id;
  protected String iss;
  @XmlElement
  protected String club;
  protected String teamAfkorting;
  protected int teamNummer;
  protected String shirt;

  public Team(String teamNaam, String teamCode, String poule, String competitie) {
    super();
    this.teamNaam = teamNaam;
    this.teamCode = teamCode;
    this.poule = poule;
    this.competitie = competitie;
  }

  /**
   * @return the teamNaam
   */
  public String getTeamNaam() {
    return teamNaam;
  }

  /**
   * @param teamNaam the teamNaam to set
   */
  public void setTeamNaam(String teamNaam) {
    this.teamNaam = teamNaam;
  }

  /**
   * @return the competitie
   */
  public String getCompetitie() {
    return competitie;
  }

  /**
   * @param competitie the competitie to set
   */
  public void setCompetitie(String competitie) {
    this.competitie = competitie;
  }

  /**
   * @return the naam
   */
  public String getNaam() {
    return teamNaam;
  }

  /**
   * @param naam the naam to set
   */
  public void setNaam(String teamNaam) {
    this.teamNaam = teamNaam;
  }

  /**
   * @return the teamCode
   */
  public String getTeamCode() {
    return teamCode;
  }

  /**
   * @param teamCode the teamCode to set
   */
  public void setTeamCode(String teamCode) {
    this.teamCode = teamCode;
  }

  /**
   * @return the Poule
   */
  public String getPoule() {
    return poule;
  }

  /**
   * @param Poule the Poule to set
   */
  public void setPoule(String poule) {
    this.poule = poule;
  }

  @Override
  public String toString() {
    return "Team [teamNaam=" + teamNaam + ", teamCode=" + teamCode + ", poule=" + poule + ", competitie=" + competitie
	+ ", id=" + id + ", iss=" + iss + ", club=" + club + ", teamAfkorting=" + teamAfkorting + ", teamNummer="
	+ teamNummer + ", shirt=" + shirt + "]";
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getIss() {
    return iss;
  }

  public void setIss(String iss) {
    this.iss = iss;
  }

  public String getClub() {
    return club;
  }

  public void setClub(String club) {
    this.club = club;
  }

  public String getTeamAfkorting() {
    return teamAfkorting;
  }

  public void setTeamAfkorting(String teamAfkorting) {
    this.teamAfkorting = teamAfkorting;
  }

  public int getTeamNummer() {
    return teamNummer;
  }

  public void setTeamNummer(int teamNummer) {
    this.teamNummer = teamNummer;
  }

  public String getShirt() {
    return shirt;
  }

  public void setShirt(String shirt) {
    this.shirt = shirt;
  }

}