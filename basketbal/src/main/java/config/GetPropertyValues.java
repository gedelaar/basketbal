package config;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author Crunchify.com
 * 
 */

public class GetPropertyValues {

  String result = "";
  InputStream inputStream;
  File resourcesDirectory;

  
  public GetPropertyValues() {
    super();
  }


  public final String getPropValues(String property) {
 
    try {
      Properties prop = new Properties();
      String propFileName = "resources/config.properties";
     // System.out.println(resourcesDirectory.getAbsolutePath());

      System.out.println(getClass().getProtectionDomain().getCodeSource().getLocation().getPath());
      System.out.println(getClass().getClassLoader().getName());
      inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

      if (inputStream != null) {
	prop.load(inputStream);
      }
      else {
	throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
      }

      result = prop.getProperty(property);

    }
    catch (Exception e) {
      System.out.println("Exception: " + e);
    }
    finally {
      try {
	inputStream.close();
      }
      catch (IOException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
      }
    }
    return result;
  }

}
