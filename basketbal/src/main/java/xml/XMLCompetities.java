package xml;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import config.GetPropertyValues;

public class XMLCompetities {
  Competities competities;
  JAXBContext jaxbContext;
  File resourcesDirectory = null;

  public XMLCompetities() {
    super();
  }

  public void readXML() {
    GetPropertyValues properties = new GetPropertyValues();
    {
      try {
	jaxbContext = JAXBContext.newInstance(Competities.class);
	Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

	URL url = (new URI(properties.getPropValues("urlCompetities")).toURL());
	competities = (Competities) jaxbUnmarshaller.unmarshal(url);

      }
      catch (JAXBException e) {
	e.printStackTrace();
      }
      catch (MalformedURLException e) {
	e.printStackTrace();
      }
      catch (URISyntaxException e) {
	e.printStackTrace();
      }
    }
  }

  public File getResourcesDirectory() {
    return resourcesDirectory;
  }

  public void setResourcesDirectory(File resourcesDirectory) {
    this.resourcesDirectory = resourcesDirectory;
  }

}
