package xml;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "competities")
@XmlAccessorType(XmlAccessType.FIELD)
public class Competities {

  // private static final long serialVersionUID = 1L;

  @XmlElement(name = "organisatie")
  private List<Organisatie> organisaties;

  public List<Organisatie> getOrganisatieList() {
    return organisaties;
  }

  public void setOrganisatieList(List<Organisatie> organisaties) {
    this.organisaties = organisaties;
  }

  

  public Competities() {
    super();
  }

  @Override
  public String toString() {
    return "Competities [organisaties=" + organisaties + "]";
  }

  

 
}
