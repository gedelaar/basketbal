package xml;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonGetter;

import wedstrijd.ThuisWedstrijd;

@XmlRootElement(name = "competitie")
@XmlAccessorType(XmlAccessType.FIELD)
public class Competitie implements Serializable{

  private static final long serialVersionUID = 1L;

  @XmlAttribute
  private int id;
  private String nummer;
  @XmlAttribute
  private String organisatieid;

  @XmlElement(name = "wedstrijd")
  private List<ThuisWedstrijd> wedstrijden;


  public List<ThuisWedstrijd> getWedstrijdList() {
    return wedstrijden;
  }

  public void setWedstrijdList(List<ThuisWedstrijd> wedstrijden) {
    this.wedstrijden = wedstrijden;
  }

  private String iss;
  private String naam;
  private String standen;
  private String uitslagen;
  private String gewijzigd;

  public Competitie() {
    super();
  }

  public String getNummer() {
    return nummer;
  }

  public void setNummer(String nummer) {
    this.nummer = nummer;
  }

  public String getIss() {
    return iss;
  }

  public void setIss(String iss) {
    this.iss = iss;
  }

  @Override
  public String toString() {
    return "Competitie [id=" + id + ", nummer=" + nummer + ", organisatieid=" + organisatieid + ", wedstrijden="
	+ wedstrijden + ", iss=" + iss + ", naam=" + naam + ", standen=" + standen + ", uitslagen=" + uitslagen
	+ ", gewijzigd=" + gewijzigd + "]";
  }

  public String getNaam() {
    return naam;
  }

  public void setNaam(String naam) {
    this.naam = naam;
  }

  public String getStanden() {
    return standen;
  }

  public void setStanden(String standen) {
    this.standen = standen;
  }

  public String getUitslagen() {
    return uitslagen;
  }

  public void setUitslagen(String uitslagen) {
    this.uitslagen = uitslagen;
  }

  public String getGewijzigd() {
    return gewijzigd;
  }

  public void setGewijzigd(String gewijzigd) {
    this.gewijzigd = gewijzigd;
  }

  public String getOrganisatieid() {
    return organisatieid;
  }

  public void setOrganisatieid(String organisatieid) {
    this.organisatieid = organisatieid;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

}
