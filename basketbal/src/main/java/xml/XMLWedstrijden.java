package xml;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import config.GetPropertyValues;
import wedstrijd.Wedstrijden;

public class XMLWedstrijden {

  Wedstrijden wedstrijden;
  JAXBContext jaxbContext;
  GetPropertyValues properties = new GetPropertyValues();
  {
    try {
      jaxbContext = JAXBContext.newInstance(Wedstrijden.class);
      Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
      URL url = (new URI(properties.getPropValues("urlWedstrijden")).toURL());
      wedstrijden = (Wedstrijden) jaxbUnmarshaller.unmarshal(url);
      System.out.println(wedstrijden.getSeizoen());

    }
    catch (JAXBException e) {
      e.printStackTrace();
    }
    catch (MalformedURLException e) {
      e.printStackTrace();
    }
    catch (URISyntaxException e) {
      e.printStackTrace();
    }

  }

}
