package xml;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "organisatie")
@XmlAccessorType(XmlAccessType.FIELD)
public class Organisatie implements Serializable {

  private static final long serialVersionUID = 1L;

  private String naam;
  @XmlAttribute
  private String id;
  
  @XmlElement(name = "competitie")
  private List<Competitie> competities;
  
  public List<Competitie> getcompetitieList() {
    return competities;
  }

  public void setOrganisatieList(List<Competitie> competities) {
    this.competities = competities;
  }

  public Organisatie() {
    super();
  }

  public String getNaam() {
    return naam;
  }

  public void setNaam(String naam) {
    this.naam = naam;
  }

  @Override
  public String toString() {
    return "Organisatie [naam=" + naam + ", id=" + id + ", competities=" + competities + "]";
  }

  public String getIds() {
    return id;
  }

  public void setIds(String ids) {
    this.id = ids;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  
}
