package xml;

import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Demo {
  DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
  {

    try {
      DocumentBuilder builder = builderFactory.newDocumentBuilder();
      Document doc = builder.parse("e:/competities.xml");
      NodeList namelist = (NodeList) doc.getElementById("2");
      System.out.println(doc.getTextContent());

      for (int i = 0; i < namelist.getLength(); i++) {
	Node p = namelist.item(i);

	if (p.getNodeType() == Node.ELEMENT_NODE) {
	  Element person = (Element) p;
	  NodeList id = (NodeList) person.getElementsByTagName("competitie");
	  NodeList nodeList = person.getChildNodes();
	  // List<EmployeeDto> employeeDtoList = new ArrayList();

	  for (int j = 0; j < nodeList.getLength(); j++) {
	    Node n = nodeList.item(j);

	    if (n.getNodeType() == Node.ELEMENT_NODE) {
	      Element naame = (Element) n;
	      System.out.println("Employee" + id + ":" + naame.getTagName() + "=" + naame.getTextContent());
	    }
	  }
	}
      }
    }
    catch (ParserConfigurationException e) {
      e.printStackTrace();
    }
    catch (SAXException e) {
      e.printStackTrace();
    }
    catch (IOException e) {
      e.printStackTrace();
    }
  }
}
