package persoon;

public abstract class Persoon {
  protected String naam;
  protected String id;

  @Override
  public String toString() {

    return "id = " + this.getId() + " naam = " + this.getNaam();
  }

  public String getNaam() {
    return naam;
  }

  /**
   * @param naam the naam to set
   */
  public void setNaam(String naam) {
    this.naam = naam;
  }

  public String getId() {
    return id;
  }

}
