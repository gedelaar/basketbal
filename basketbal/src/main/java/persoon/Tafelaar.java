package persoon;

public class Tafelaar extends Persoon {
  private int niveau;

  public Tafelaar(String naam, int niveau) {
    super();
    this.naam = naam;
    this.niveau = niveau;
  }

  /**
   * @return the niveau
   */
  public int getNiveau() {
    return niveau;
  }

  /**
   * @param niveau the niveau to set
   */
  public void setNiveau(int Niveau) {
    this.niveau = Niveau;
  }

  @Override
  public String toString() {
    return "niveau " + getNiveau() + " naam = " + getNaam();
  }
}
