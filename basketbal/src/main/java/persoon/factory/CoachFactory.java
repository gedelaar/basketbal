package persoon.factory;

import persoon.Coach;
import persoon.Persoon;

public class CoachFactory extends PersoonFactory {

  public CoachFactory(String naam, String lidnr) {
    super();
    super.naam = naam;
    super.lidNummer = lidnr;
  }

  @Override
  public Persoon createPerson() {
    return new Coach(naam, lidNummer);
  }

}
