package persoon.factory;

import persoon.Persoon;

public interface PersoonAbstractFactory {

  public Persoon createPerson();

}
