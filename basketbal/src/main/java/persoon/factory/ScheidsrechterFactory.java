package persoon.factory;

import persoon.Persoon;
import persoon.Scheidsrechter;

public class ScheidsrechterFactory extends PersoonFactory {

  private String niveau;

  public ScheidsrechterFactory(String naam, String niveau) {
    super();
    setNiveau(niveau);
    setNaam(naam);
  }

  private void setNiveau(String value) {
    this.niveau = value;
  }

  private String getNiveau() {
    return this.niveau;
  }

  @Override
  public Persoon createPerson() {
    return new Scheidsrechter(naam, niveau);
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "RefereeFactory [niveau=" + getNiveau() + ", naam=" + getNaam() + ", lidNummer=" + getLidNummer() + "]";
  }
}
