package persoon.factory;

import persoon.Persoon;
import persoon.Tafelaar;

public class TafelaarFactory extends PersoonFactory {

  private int niveau;

  public TafelaarFactory(String naam, int niveau) {
    super();
    setNiveau(niveau);
    setNaam(naam);
  }

  private void setNiveau(int value) {
    this.niveau = value;
  }

  private int getNiveau() {
    return this.niveau;
  }

  @Override
  public Persoon createPerson() {
    return new Tafelaar(naam, niveau);
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "TablerFactory [niveau=" + niveau + ", naam=" + naam + ", lidNummer=" + lidNummer + ", getNiveau()="
	+ getNiveau() + ", getNaam()=" + getNaam() + ", getLidNummer()=" + getLidNummer() + "]";
  }

}
