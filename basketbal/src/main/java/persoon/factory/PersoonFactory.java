package persoon.factory;

import persoon.Persoon;

public abstract class PersoonFactory implements PersoonAbstractFactory {

  protected String naam;
  protected String lidNummer;

  public static Persoon getPerson(PersoonAbstractFactory factory) {
    return factory.createPerson();
  }

  /**
   * @return the naam
   */
  public String getNaam() {
    return naam;
  }

  /**
   * @param naam the naam to set
   */
  public void setNaam(String naam) {
    this.naam = naam;
  }

  /**
   * @return the lidNummer
   */
  public String getLidNummer() {
    return lidNummer;
  }

  /**
   * @param lidNummer the lidNummer to set
   */
  public void setLidNummer(String lidNummer) {
    this.lidNummer = lidNummer;
  }

  public Persoon createPerson() {
    return null;
  }
}
