package persoon.factory;

import persoon.Lid;

public class LidFactory extends PersoonFactory {

  private String woonplaats;

  public LidFactory(String lidNummer, String naam, String woonplaats) {
    super();
    setLidNummer(lidNummer);
    setNaam(naam);
    setWoonplaats(woonplaats);
  }

  private void setWoonplaats(String value) {
    this.woonplaats = value;
  }

  private String getWoonplaats() {
    return this.woonplaats;
  }

  @Override
  public Lid createPerson() {
    return new Lid(naam, lidNummer, woonplaats);
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "LidFactory [woonplaats=" + woonplaats + ", naam=" + naam + ", lidNummer=" + lidNummer + ", getWoonplaats()="
	+ getWoonplaats() + ", getNaam()=" + getNaam() + ", getLidNummer()=" + getLidNummer() + "]";
  }
}
