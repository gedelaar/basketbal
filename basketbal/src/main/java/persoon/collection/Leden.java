/**
 * 
 */
package persoon.collection;

import java.util.ArrayList;

import persoon.Lid;
import persoon.Personen;
import persoon.Persoon;

/**
 * @author gerard
 *
 */
public class Leden extends Personen {

  public Leden() {
    super();
  }
  
  public void addLid(Lid lid) {
    personen.add(lid);
  }


  public Lid getLid(int id) {
    if (personen.size() > id) {
      return (Lid) personen.get(id);
    }
    else
      return null;
  }

  /*
   * (non-Javadoc)
   * 
   * @see persoon.Personen#personen()
   * 
   */
  @Override
  public ArrayList<Persoon> personen() {
    return super.personen;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "Leden [personen=" + personen + "]";
  }

}
