package persoon.collection;

import java.util.ArrayList;

import persoon.Coach;
import persoon.Personen;
import persoon.Persoon;

public class Coaches extends Personen {

  public Coaches() {
    super();
    // TODO Auto-generated constructor stub
  }

  @Override
  public ArrayList<Persoon> personen() {
      return super.personen();
  }

  public void addCoach(Coach coach) {
    personen.add(coach);
  }

  public Coach getCoaches(int id) {
    if (personen.size() > id) {
      return (Coach) personen.get(id);
    }
    else
      return null;
  }

  @Override
  public String toString() {
    return "coaches [personen=" + personen + "]";
  }

}
