package persoon;

public class Lid extends Persoon {
  private String lidNummer;
  private String woonplaats;

  public Lid(String naam, String lidnr, String woonplaats) {
    super();
    super.naam = naam;
    this.lidNummer = lidnr;
    this.woonplaats = woonplaats;
  }

  
  /**
   * @return the lidnr
   */
  public String getLidNummer() {
    return lidNummer;
  }

  public String getWoonplaats() {
    return this.woonplaats;
  }

  /*
   * (non-Javadoc)
   * 
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "Lid [lidNummer=" + lidNummer + ", woonplaats=" + woonplaats + ", naam=" + naam + "]";
  }

}
