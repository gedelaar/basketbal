package persoon;

public class Scheidsrechter extends Persoon {

  private String niveau;

  public Scheidsrechter(String naam, String niveau) {
    super();
    this.niveau = niveau;
    super.naam = naam;
  }

  /**
   * @return the niveau
   */
  public String getNiveau() {
    return niveau;
  }

  /**
   * @param niveau the niveau to set
   */
  public void setNiveau(String niveau) {
    this.niveau = niveau;
  }

  @Override
  public String toString() {
    return "niveau " + getNiveau() + " naam = " + getNaam();
  }
}
