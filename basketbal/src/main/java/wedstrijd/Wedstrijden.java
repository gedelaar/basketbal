package wedstrijd;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonGetter;

import xml.Competitie;

@XmlRootElement(name = "wedstrijden")
@XmlAccessorType(XmlAccessType.FIELD)
public class Wedstrijden {

  String seizoen;
  @XmlElement(name = "competitie")
  private List<Competitie> competities;

  @JsonGetter("wedstrijden")
  public List<Competitie> getCompetitieList() {
    return competities;
  }

  public void setCompetitieList(List<Competitie> competities) {
    this.competities = competities;
  }

  public String getSeizoen() {
    return seizoen;
  }

  public void setSeizoen(String seizoen) {
    this.seizoen = seizoen;
  }

  @Override
  public String toString() {
    return "Wedstrijden [seizoen=" + seizoen + ", competities=" + competities + "]";
  }

 

}
