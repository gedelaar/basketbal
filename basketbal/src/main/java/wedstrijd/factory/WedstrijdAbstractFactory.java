package wedstrijd.factory;

import wedstrijd.Wedstrijd;

public interface WedstrijdAbstractFactory {

  public Wedstrijd createWedstrijd();
}
