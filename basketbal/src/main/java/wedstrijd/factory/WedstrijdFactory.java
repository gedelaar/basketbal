package wedstrijd.factory;

import java.util.Date;

import team.OponentTeam;
import team.OwnTeam;
import wedstrijd.Wedstrijd;

public class WedstrijdFactory implements WedstrijdAbstractFactory {

  protected Date datum;
  protected String poule;
  protected long tijd;
  protected String code;
  protected OwnTeam thuisTeam;
  protected OponentTeam uitTeam;

  public static Wedstrijd getWedstrijd(WedstrijdAbstractFactory factory) {
    return factory.createWedstrijd();
  }

  public Wedstrijd createWedstrijd() {
    return null;
  }
}
