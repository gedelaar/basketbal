package wedstrijd.factory;

import java.util.Date;

import team.OponentTeam;
import team.OwnTeam;
import wedstrijd.ThuisWedstrijd;
import wedstrijd.Wedstrijd;

public class ThuisWedstrijdFactory extends WedstrijdFactory {

  public ThuisWedstrijdFactory(Date datum, String poule, long tijd, String code, OwnTeam thuisTeam,
      OponentTeam uitTeam) {
    super();
    super.datum = datum;
    super.poule = poule;
    super.tijd = tijd;
    super.code = code;
    super.thuisTeam = thuisTeam;
    super.uitTeam = uitTeam;
  }

  public Wedstrijd createWedstrijd() {

    return new ThuisWedstrijd(datum, poule, tijd, code, thuisTeam, uitTeam);
  }

}
