package wedstrijd;

import java.util.Date;

import team.*;

public class ThuisWedstrijd extends Wedstrijd {

  public ThuisWedstrijd() {
    super();
    // TODO Auto-generated constructor stub
  }

  public ThuisWedstrijd(Date datum, String poule, long tijd, String code, OwnTeam thuisTeam, OponentTeam uitTeam) {
    super();
    super.datum = datum;
    super.poule = poule;
    super.tijd = tijd;
    super.code = code;
    super.thuisTeam = thuisTeam;
    super.uitTeam = uitTeam;
  }

  @Override
  public OwnTeam getThuisTeam() {
    return thuisTeam;
  }

  @Override
  public OponentTeam getUitTeam() {
    return uitTeam;
  }

  @Override
  public String getOfficial() {
    return null;
  }

}
