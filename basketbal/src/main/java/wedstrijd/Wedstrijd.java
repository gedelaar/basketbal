package wedstrijd;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import team.OponentTeam;
import team.OwnTeam;

@XmlRootElement(name = "wedstrijd")
@XmlAccessorType(XmlAccessType.FIELD)
public abstract class Wedstrijd {

  @XmlAttribute
  protected int id;
  @XmlElement()
  protected String nummer;
  
  @XmlElement(name = "thuisploeg")
  protected OwnTeam thuisTeam;
  @XmlElement (name = "uitploeg")
  protected OponentTeam uitTeam;
  protected Date datum;
  protected String poule;
  protected long tijd;
  protected String code;

  public Wedstrijd() {
    super();
  }

 
  public abstract OwnTeam getThuisTeam();

  public abstract OponentTeam getUitTeam();

  public abstract String getOfficial();

  public  Date getDatum() {
    return datum;
  }

  public String getPoule() {
    return poule;
  }

  public long getTijd() {
    return tijd;
  }

  public String getCode() {
    return code;
  }

  @Override
  public String toString() {
    return "ThuisWedstrijd [Datum=" + datum + ", Poule=" + poule + ", Tijd=" + tijd + ", Code=" + code + ", ThuisTeam="
	+ thuisTeam + ", UitTeam=" + uitTeam + "]";
  }

  public String getNummer() {
    return nummer;
  }

  public void setNummer(String nummer) {
    this.nummer = nummer;
  }


  public int getId() {
    return id;
  }


  public void setId(int id) {
    this.id = id;
  }


  public void setThuisTeam(OwnTeam thuisTeam) {
    this.thuisTeam = thuisTeam;
  }


  public void setUitTeam(OponentTeam uitTeam) {
    this.uitTeam = uitTeam;
  }


  public void setDatum(String datum) throws ParseException {
    Date date1=new SimpleDateFormat("dd-MM-yyyy").parse(datum); 
    this.datum = date1;
  }


  public void setPoule(String poule) {
    this.poule = poule;
  }


  public void setTijd(long tijd) {
    this.tijd = tijd;
  }


  public void setCode(String code) {
    this.code = code;
  }

}
