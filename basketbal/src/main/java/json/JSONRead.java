package json;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Scanner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JSONRead {

  public static void main(String[] args) throws JSONException, URISyntaxException, IOException {
    String s = "http://db.basketball.nl/db/json/club.pl";
    URL url = (new URI(s).toURL());

    // read from the URL
    Scanner scan = new Scanner(url.openStream());
    String str = new String();
    while (scan.hasNext())
      str += scan.nextLine();
    scan.close();

    // System.out.println(str);

    JSONObject obj = new JSONObject(str);

    JSONArray arr = obj.getJSONArray("clubs");

    for (int i = 0; i < arr.length(); i++) {
      JSONObject a = (JSONObject) arr.get(i);
      if (a.get("naam").equals("Forwodians")) {
	String n = a.getString("naam");
	int id = a.getInt("id");
	System.out.println(n + "  " + id); // prints "Alice 20"
      }

    }

  }

}
