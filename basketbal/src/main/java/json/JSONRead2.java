package json;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Scanner;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import wedstrijd.Wedstrijden;

public class JSONRead2 {

  public static void main(String[] args) throws URISyntaxException, IOException {
    final ObjectMapper objectMapper = new ObjectMapper();

    String s = "http://db.basketball.nl/db/json/wedstrijd.pl?clb_ID=96";
    URL url = (new URI(s).toURL());

   // read from the URL
    Scanner scan = new Scanner(url.openStream());
    String str = new String();
    while (scan.hasNext())
      str += scan.nextLine();
    scan.close();
    System.out.println(str);

    objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    objectMapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
    // Read JSON file and convert to java object
    // InputStream fileInputStream = new FileInputStream("post.json");
    Wedstrijden  wedstrijden = objectMapper.readValue(url, Wedstrijden.class);
    //objectMapper.
    // fileInputStream.close();

    System.out.println(wedstrijden);
  }

}
