package basketbal_test;

import java.util.ArrayList;
import java.util.Collection;

import persoon.Persoon;
import persoon.factory.CoachFactory;
import persoon.factory.LidFactory;
import persoon.factory.PersoonFactory;
import persoon.factory.ScheidsrechterFactory;
import persoon.factory.TafelaarFactory;

public class TestPersonAbstractFactory {

  public static void main(String[] args) {
    testPersonAbstractFactory();

  }

  private static void testPersonAbstractFactory() {
    Persoon lid = PersoonFactory.getPerson(new LidFactory("D84f", "gerard", "voorhout"));
    Persoon coach = PersoonFactory.getPerson(new CoachFactory("henk", "D87J"));
    Persoon referee = PersoonFactory.getPerson((new ScheidsrechterFactory("Piet", "A")));
    Persoon tabler = PersoonFactory.getPerson(new TafelaarFactory("Jan", 2));
    System.out.println(lid);
    System.out.println(coach);
    System.out.println(referee);
    System.out.println(tabler);
    
    Collection<Persoon> personen = new ArrayList<Persoon>();
    personen.add(coach);
    personen.add(PersoonFactory.getPerson(new ScheidsrechterFactory("Fieke", "C")));
    personen.add(tabler);
    
    for (Persoon persoon : personen) {
      System.out.println(persoon.getNaam());
      System.out.println(persoon.getClass());
      
    }
    
    System.out.println(personen);
    
    
  }
}
