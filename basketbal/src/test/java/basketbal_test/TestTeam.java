package basketbal_test;

import persoon.factory.CoachFactory;
import persoon.factory.PersoonFactory;
import team.OponentTeam;
import team.OwnTeam;
import team.Team;

public class TestTeam {

  public static void main(String[] args) {
    OwnTeam thuisTeam = new OwnTeam("H1-1", "heren 1", "DA34",
	"15",  PersoonFactory.getPerson((new CoachFactory("Jaap", "FD43"))));
    System.out.println(thuisTeam.getCoach().getNaam());
    System.out.println(thuisTeam);

    Team uitTeam = new OponentTeam("H1-1", "heren 1", "DA34", "12");
    System.out.println(uitTeam);

  }

}
