package basketbal_test;

import persoon.Coach;
import persoon.Lid;
import persoon.Persoon;
import persoon.collection.Coaches;
import persoon.collection.Leden;
import persoon.factory.CoachFactory;
import persoon.factory.LidFactory;
import persoon.factory.PersoonFactory;

public class TestLeden {

  public static void main(String[] args) {
    Leden leden = new Leden();
    leden.addLid(new Lid("dd", "rr", "tt"));
    leden.addLid(new Lid("gerard", "DH6", "Voorhout"));
    leden.addLid((Lid) PersoonFactory.getPerson(new LidFactory("D84f", "jantje", "voorhout")));
    //leden.addLid( (Lid) PersoonFactory.getPerson(new CoachFactory("pietje", "D87J")));
    
    System.out.println(leden.getLid(2));
    
    for (Persoon lid : leden.personen()) {
      System.out.println(lid.getNaam());
      System.out.println(lid.getClass());
    
    }
    Coaches coaches = new Coaches();
    coaches.addCoach((Coach) PersoonFactory.getPerson(new CoachFactory("gerard", "DD66")));
    System.out.println(coaches.getCoaches(0));

  }

}
