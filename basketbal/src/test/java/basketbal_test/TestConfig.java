package basketbal_test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

import config.GetPropertyValues;

public class TestConfig {

  public static void main(String[] args) throws IOException {
    Properties properties = new Properties();
    properties.setProperty("test", "waarde");
    FileWriter writer = new FileWriter("conf.properties");
    properties.store(writer, "Author: Gerard");
    writer.close();

    GetPropertyValues propVal = new GetPropertyValues();
    System.out.println(propVal.getPropValues("urlCompetities"));
  }
}
