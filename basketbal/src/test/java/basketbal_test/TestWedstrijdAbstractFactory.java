package basketbal_test;


import java.util.Date;

import persoon.factory.CoachFactory;
import persoon.factory.PersoonFactory;
import team.OponentTeam;
import team.OwnTeam;
import wedstrijd.Wedstrijd;
import wedstrijd.factory.ThuisWedstrijdFactory;
import wedstrijd.factory.WedstrijdFactory;

public class TestWedstrijdAbstractFactory {

  public static void main(String[] args) {
    testWedstrijdAbstractFactory();

  }

  private static void testWedstrijdAbstractFactory() {

    Date datum = new Date(20-11-2019);
    long tijd = new Date().getTime();
    
    Wedstrijd thuisWedstrijd = WedstrijdFactory.getWedstrijd(new ThuisWedstrijdFactory(datum, "1234", tijd,
	"DF2", new OwnTeam("H1-1", "heren 1 fw", "DF2", "25",  PersoonFactory.getPerson(new CoachFactory("Jaapie", "FD43"))),
	new OponentTeam("HZ1-1", "heren 1 Zoebas",  "DF2", "1")));

    System.out.println(thuisWedstrijd);
    System.out.println(thuisWedstrijd.getThuisTeam().getNaam());
    System.out.println(thuisWedstrijd.getThuisTeam().getTeamCode());
    System.out.println(thuisWedstrijd.getThuisTeam().getCoach().getNaam());
    
    

  }
}
