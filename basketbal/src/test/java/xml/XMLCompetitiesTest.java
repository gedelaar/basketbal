package xml;

import static org.junit.Assert.*;

import java.io.File;
import java.net.URISyntaxException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class XMLCompetitiesTest {

  XMLCompetities test;
  static File resourcesDirectory;

  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
   // resourcesDirectory = new File("C:\\Users\\gerard\\git\\basketbal2\\basketbal\\resources\\test");
  }

  @Before
  public void setUp() throws Exception {
   // System.out.println(resourcesDirectory.getAbsolutePath());
    test = new XMLCompetities();
    //test.setResourcesDirectory(resourcesDirectory);
    test.readXML();
  }

  @Test
  public void test() throws URISyntaxException {
    // System.out.println(this.getClass().getResource());
    // File inputXmlFile = new
    // File(this.getClass().getResource("/myxml.xml").getFile());
    // System.out.println(inputXmlFile.getPath());
    //File config = Paths.get(getClass().getProtectionDomain().getCodeSource().getLocation().toURI())
	//.resolve(Paths.get("config.properties")).toFile();
    // System.out.println(config.getAbsolutePath());
    // System.out.println(resourcesDirectory.getAbsolutePath());
    System.out.println(test.competities.getOrganisatieList().get(0));
    assertEquals("test", "x", test.competities.getOrganisatieList().get(0).getNaam());
  }

}
