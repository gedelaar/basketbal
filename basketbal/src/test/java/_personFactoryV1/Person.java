package _personFactoryV1;

public abstract class Person {
  public abstract String getNaam();

  public abstract String getId();

 
  @Override
  public String toString() {

    return "id = " + this.getId() + " naam = " + this.getNaam();
  }

}
