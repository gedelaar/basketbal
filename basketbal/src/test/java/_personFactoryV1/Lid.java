package _personFactoryV1;

public class Lid extends Person {
  private String Naam;
  private String Lidnr;
  private String Woonplaats;

  public Lid(String naam, String lidnr, String woonplaats) {
    super();
    this.Naam = naam;
    this.Lidnr = lidnr;
    this.Woonplaats = woonplaats;
  }

  /**
   * @return the lidnr
   */
  public String getLidnr() {
    return Lidnr;
  }

  /**
   * @param lidnr the lidnr to set
   */
  public void setLidnr(String lidnr) {
    Lidnr = lidnr;
  }

  @Override
  public String getNaam() {
    // TODO Auto-generated method stub
    return this.Naam;
  }

  @Override
  public String getId() {
    // TODO Auto-generated method stub
    return this.Lidnr;
  }


  public String getWoonplaats() {
    // TODO Auto-generated method stub
    return this.Woonplaats;
  }

}
