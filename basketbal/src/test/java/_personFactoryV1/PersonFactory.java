package _personFactoryV1;

public class PersonFactory {
  public static Person getNaam(String Naam, String Id, String Woonplaats, String Type) {
    if ("Lid".equalsIgnoreCase(Type))
      return new Lid(Naam, Id, Woonplaats);
    else
      return new Coach(Naam, Id);

  }
}
