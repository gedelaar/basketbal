package _personFactoryV1;

public class TestPersonFactory {

  public static void main(String[] args) {
    Person lid = PersonFactory.getNaam("gerard", "D93F", "Voorhout", "lid");
    System.out.println(lid);
    Person coach = PersonFactory.getNaam("pietje", "D88A", "Voorhout", "Coach");
    System.out.println(coach);
  }

}
