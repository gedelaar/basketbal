package persoon;

import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;

import persoon.factory.CoachFactory;
import persoon.factory.PersoonFactory;

public class CoachFactoryTest {

  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
  }

  @Test
  public void coachFactoryTest() {
    String naam = "Jan";
    String id = "DA22";
    PersoonFactory coachFactory = new CoachFactory(naam,id);
    assertEquals("naam gevonden",naam ,coachFactory.createPerson().getNaam());
    assertEquals("Id gevonden",id, coachFactory.createPerson().getId());
  }

}
