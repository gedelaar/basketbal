package persoon;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

import persoon.Coach;
import persoon.Persoon;

public class CoachTest {
  private static Persoon coach;

  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    coach = new Coach("Piet", "DE43");
  }

  @Test
  public void testGetNaam() {
    assertEquals("Piet gevonden", "Piet", coach.getNaam());
    assertNotEquals("Piet niet gevonden", "Pietje", coach.getNaam());
  }

  @Test
  public void testGetId() {
    assertEquals("DE43 gevonden", "DE43", coach.getId());
    assertNotEquals("DE43 niet gevonden", "DE44", coach.getId());

  }

  @Test
  public void testCoach() {
    Persoon coach;
    coach = new Coach("xyz", "DE99");
    assertEquals("DE99 gevonden", "DE99", coach.getId());
    assertEquals("xyz gevonden", "xyz", coach.getNaam());
  }

}
